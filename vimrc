:syntax on
:colorscheme elflord
:set relativenumber 
:set number 

:noremap ; :
:noremap : ; 
:imap jj <Esc>
:map M <plug>NERDTreeTabsToggle<CR> 
:set tabstop=8
:set shiftwidth=8
:set softtabstop=8
:set noexpandtab
:set title
:set autoindent
:set smartindent
:set cindent
:set mouse=n

:set nocp
execute pathogen#infect()
filetype plugin indent on

highlight ColorColumn ctermbg=darkred
call matchadd('ColorColumn', '\%81v', 100)

call camelcasemotion#CreateMotionMappings(',')

augroup AutoSaveFolds
	autocmd!
	autocmd BufWinLeave,BufLeave,BufWritePost ?* nested silent! mkview!
	autocmd BufWinEnter ?* silent! loadview
augroup end

:set viewoptions=folds,cursor
:set sessionoptions=folds

let g:notes_directories = [ '~/.notes/src' ]

let noteFiles = "~/.notes/src/*"
execute "autocmd BufEnter " . noteFiles . " set textwidth=80"
execute "autocmd BufLeave " . noteFiles . " set textwidth=0"
unlet noteFiles

:command Json %!python -m json.tool
